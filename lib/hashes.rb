# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  hsh = {}
  str.split(' ').each{ |wrd| hsh[wrd] = wrd.length }
  hsh
end


# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by{|k,v|v}.last[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each{ |k,v| older[k] = v }
  older
end



# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  hsh = Hash.new(0)
  word.chars.each{ |char| hsh[char] += 1 }
  hsh
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hsh = Hash.new(0)
  arr.each{|el| hsh[el] += 1}
  hsh.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hsh = {even: 0, odd: 0}
  numbers.each{|num| num % 2 == 0 ? hsh[:even] += 1 : hsh[:odd] += 1 }
  hsh
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  hsh = Hash.new(0)
  string.chars.each{ |char| hsh[char] += 1 if char =~ /[aeiou]/ }
  val = hsh.sort_by{|k,v|v}.last[1]
  hsh.sort_by{|k,v|k}.to_h.key(val)

end



# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  students.select{|stu,bmo|(bmo>=7)&&(bmo<=12)}.keys.combination(2).to_a
end



# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  hsh = Hash.new(0)
  specimens.each{ |spe| hsh[spe] += 1}
  arr = hsh.sort_by{|k,v|v}
  (hsh.keys.count ** 2) * arr.first[1] / arr.last[1]
end



# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(nsign, vsign)
  cchash(vsign).all?{ |k,v| cchash(nsign).has_key?(k) && (cchash(nsign)[k] >= v) }
end

def cchash(str)
  str.downcase!
  str.delete!(' ')
  hsh = Hash.new(0)
  str.chars.each{ |char| hsh[char] += 1 }
  hsh
end




=begin

1. can ignore capitalization and punctuation


>=

1. get all the letters and their respective frequencies in the vandalized_sign

2. compare with the biz_sign. true only if they have all the letters and the count is >= the vandalized sign.


=end
